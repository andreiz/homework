package main

import (
	"fincompare/pkg/sink"

	"github.com/cenkalti/backoff"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	nsq "github.com/segmentio/nsq-go"
	"github.com/spf13/viper"
)

var (
	cmd *cobra.Command
)

const (
	maxInFlight = 300
	maxRetries  = 8
)

func main() {
	if err := cmd.Execute(); err != nil {
		logrus.WithError(err).Fatal("Failed - stop messing up")
	}
}

func init() {
	cmd = &cobra.Command{
		Use:   "sink",
		Short: "sink listens for nsq messages and saves the data received",
		Long:  "sink listens for nsq messages and reacts to users.create topic by persisting users to the database",
		Run:   runFunc,
	}

	cmd.Flags().String("dsn", "postgres://postgres@db:5432/users?sslmode=disable", "Database connection address (DSN)")
	cmd.Flags().String("nsq-addr", "nsqlookupd:4161", "nsqd address")
	cmd.Flags().String("nsq-topic", "users.create", "nsq topic")
	cmd.Flags().String("nsq-chan", "sink", "nsq channel")

	viper.BindPFlags(cmd.Flags())

	viper.SetEnvPrefix("") // TODO needed?
	viper.AutomaticEnv()
}

func runFunc(cmd *cobra.Command, args []string) {
	log := logrus.WithFields(logrus.Fields{
		"dsn":      viper.GetString("dsn"),
		"nsqAddr":  viper.GetString("nsq-addr"),
		"nsqTopic": viper.GetString("nsq-topic"),
		"nsqChan":  viper.GetString("nsq-chan"),
	})
	log.Info("Starting...")

	var (
		db       *sqlx.DB
		attempts int
	)

	// Set up DB connection
	if err := backoff.Retry(func() error {
		var innerErr error

		attempts++

		if innerErr = (&nsq.Client{
			Address: viper.GetString("nsq-addr"),
		}).CreateChannel(
			viper.GetString("nsq-topic"),
			viper.GetString("nsq-chan"),
		); innerErr != nil {
			log.WithField("attempts", attempts).WithError(innerErr).Warn("Failed to create nsq topic")
			return errors.Wrap(innerErr, "failed to create topic")
		}


		if db, innerErr = sqlx.Connect(
			"postgres", viper.GetString("dsn"),
		); innerErr != nil {
			log.WithError(innerErr).WithField("attempts", attempts).Warn("Failed to connect to the database")
			return errors.Wrap(innerErr, "failed to connect to the database")
		}

		return nil
	}, backoff.WithMaxRetries(backoff.NewExponentialBackOff(), maxRetries)); err != nil {
		log.WithError(err).Fatal("Failed to connect to the database")
	}

	// Create Sink
	sink, err := sink.New(db,
		viper.GetString("nsq-topic"),
		viper.GetString("nsq-chan"),
		viper.GetString("nsq-addr"),
		maxInFlight,
	)
	if err != nil {
		log.WithError(err).Fatal("Failed to initialize Sink")
	}

	// Block listening for messages.
	if err := sink.Consume(); err != nil {
		log.WithError(err).Fatal("sink.Consume failed")
	}
	logrus.Info("Stopped gracefully")
}
