package main

import (
	"os"

	"fincompare/pkg/csvimporter"

	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
	nsq "github.com/segmentio/nsq-go"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	maxRetries = 10
)

var (
	cmd *cobra.Command
)

func main() {
	if err := cmd.Execute(); err != nil {
		logrus.WithError(err).Fatal("Failed - who you gonna call?")
	}
}

func init() {
	cmd = &cobra.Command{
		Use:   "csvimporter",
		Short: "Read user records from CSV and publish them to nsq",
		Long:  `Imports CSVs by sending them over nsq to a data sink to be persisted to the database`,
		Run:   runFunc,
	}

	cmd.Flags().String("csv-file", "testdata/data.csv", "Default CSV file to read")
	cmd.Flags().String("nsq-addr", "nsqlookupd:4161", "nsqd address")
	cmd.Flags().String("nsq-topic", "users.create", "nsq topic")

	viper.BindPFlags(cmd.Flags())

	viper.SetEnvPrefix("") // TODO needed?
	viper.AutomaticEnv()
}

func runFunc(cmd *cobra.Command, args []string) {
	log := logrus.WithFields(logrus.Fields{
		"csv-file":  viper.GetString("csv-file"),
		"nsq-addr":  viper.GetString("nsq-addr"),
		"nsq-topic": viper.GetString("nsq-topic"),
	})
	log.Info("Reading file")

	rc, err := os.Open(viper.GetString("csv-file"))
	if err != nil {
		log.WithError(err).Fatal("Failed to read the CSV file")
	}

	producer, err := nsq.StartProducer(nsq.ProducerConfig{
		Topic:   viper.GetString("nsq-topic"),
		Address: viper.GetString("nsq-addr"),
	})
	if err != nil {
		log.WithError(err).Fatal("Failed to start nsq producer")
	}
	defer producer.Stop()

	var (
		importer *csvimporter.Importer
		attempts int
	)

	if err = backoff.Retry(func() error {
		var innerErr error

		attempts++

		if innerErr = (&nsq.Client{
			Address: viper.GetString("nsq-addr"),
		}).CreateTopic(
			viper.GetString("nsq-topic"),
		); err != nil {
			log.WithField("attempts", attempts).WithError(innerErr).Warn("Failed to create nsq topic")
			return errors.Wrap(innerErr, "failed to create topic")
		}

		importer, innerErr = csvimporter.New(producer)
		if innerErr != nil {
			log.WithField("attempts", attempts).WithError(innerErr).Warn("Failed to connect to nsq topic")
			return errors.Wrap(innerErr, "failed to connect to nsq topic")
		}

		return nil
	}, backoff.WithMaxRetries(
		backoff.NewExponentialBackOff(),
		maxRetries,
	)); err != nil {
		log.WithField("attempts", attempts).WithError(err).Fatal("Failed to init csvimporter")
	}

	n, err := importer.ReadCSV(rc)
	if err != nil {
		log.WithError(err).Fatalf("Encountered an error while running CSV importer. Read %d record(s)", n)
	}
	log.Infof("Published %d record(s)", n)
}
