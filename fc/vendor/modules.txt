# github.com/cenkalti/backoff v2.1.0+incompatible
github.com/cenkalti/backoff
# github.com/fsnotify/fsnotify v1.4.7
github.com/fsnotify/fsnotify
# github.com/golang-migrate/migrate/v4 v4.1.0
github.com/golang-migrate/migrate/v4
github.com/golang-migrate/migrate/v4/database/postgres
github.com/golang-migrate/migrate/v4/source/file
github.com/golang-migrate/migrate/v4/database
github.com/golang-migrate/migrate/v4/source
# github.com/hashicorp/hcl v1.0.0
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/jmoiron/sqlx v1.2.0
github.com/jmoiron/sqlx
github.com/jmoiron/sqlx/reflectx
# github.com/konsorten/go-windows-terminal-sequences v1.0.1
github.com/konsorten/go-windows-terminal-sequences
# github.com/lib/pq v1.0.0
github.com/lib/pq
github.com/lib/pq/oid
# github.com/magiconair/properties v1.8.0
github.com/magiconair/properties
# github.com/mitchellh/mapstructure v1.1.2
github.com/mitchellh/mapstructure
# github.com/pelletier/go-toml v1.2.0
github.com/pelletier/go-toml
# github.com/pkg/errors v0.8.0
github.com/pkg/errors
# github.com/segmentio/nsq-go v1.2.2
github.com/segmentio/nsq-go
# github.com/sirupsen/logrus v1.2.0
github.com/sirupsen/logrus
# github.com/spf13/afero v1.1.2
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.3.0
github.com/spf13/cast
# github.com/spf13/cobra v0.0.3
github.com/spf13/cobra
# github.com/spf13/jwalterweatherman v1.0.0
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.3
github.com/spf13/pflag
# github.com/spf13/viper v1.3.1
github.com/spf13/viper
# golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
golang.org/x/crypto/ssh/terminal
# golang.org/x/sys v0.0.0-20181205085412-a5c9d58dba9a
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.0
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# gopkg.in/yaml.v2 v2.2.2
gopkg.in/yaml.v2
