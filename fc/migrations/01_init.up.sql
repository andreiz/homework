CREATE TABLE "users" (
    id      BIGINT      PRIMARY KEY,

    "name"  TEXT        NOT NULL,
    email   TEXT        UNIQUE NOT NULL
);