# Code Challenge:

## Develop two separate programs that will:

### 1st Program:
Process CSV files and send each row/data to a message queue*. An example CSV file is attached.

### 2nd Program:
Consume the messages that are sent by the 1st program and insert them into a database table**.
There can only be one record with the same email address.
Imagine this program will be running on multiple servers or even on the same server with multiple processes at the same time.

* Can be any production-ready distributed system such as redis, rabbitmq, sqs, or even kafka.

** Can be any modern relational database but the solution is better to be independent of the database server.

Tips And Tricks:

- Pay attention to the details in the description.
- Develop the programs as you are developing it for a production system in a real-life scenario.
- Any programming language can be used.
