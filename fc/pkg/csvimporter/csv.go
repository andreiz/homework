package csvimporter

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"

	"fincompare/pkg/mq"

	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
	nsq "github.com/segmentio/nsq-go"
	"github.com/sirupsen/logrus"
)

const (
	maxRetries = 5
)

// Importer reads user records and sends messages to nsq.
type Importer struct {
	dest *nsq.Producer
}

// New creates a new Importer.
func New(dest *nsq.Producer) (*Importer, error) {
	if !dest.Connected() {
		return nil, errors.New("not connected to nsq")
	}
	return &Importer{
		dest: dest,
	}, nil
}

// ReadCSV reads CSV values from a ReadCloser and reports
// the number of records sent successfully and the error if any.
func (imp *Importer) ReadCSV(src io.ReadCloser) (int, error) {
	var (
		nSent  int
		err    error
		record []string
		msg    []byte
	)

	r := csv.NewReader(src)
	defer src.Close()

	for {
		// read next record
		record, err = r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nSent, errors.Wrapf(err, "failed to read record #%d", nSent+1)
		}

		log := logrus.WithFields(logrus.Fields{
			"number": nSent + 1,
			"record": fmt.Sprintf("%#v", record),
		})
		if len(record) < 2 {
			// We are expecting 2 records: name, email
			log.Error("Record doesn't match expected format")
			continue
		}

		// serialize a message
		msg, err = json.Marshal(&mq.UsersCreate{
			Name:  record[0],
			Email: record[1],
		})
		if err != nil {
			log.WithError(err).Error("Failed to serialize message")
			continue
		}

		// publish to nsq
		if err = backoff.Retry(
			func() error { return imp.dest.Publish(msg) },
			backoff.WithMaxRetries(backoff.NewExponentialBackOff(), maxRetries),
		); err != nil {
			log.WithError(err).Error("Failed to connect to the database")
			continue
		}
		nSent++
	}

	return nSent, err
}
