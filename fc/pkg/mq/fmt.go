package mq

// UsersCreate is a MQ message that creates a new user.
type UsersCreate struct {
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}
