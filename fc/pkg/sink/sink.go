package sink

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"fincompare/pkg/mq"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file" // file source for migrations
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // postgres driver
	"github.com/pkg/errors"
	nsq "github.com/segmentio/nsq-go"
	"github.com/sirupsen/logrus"
)

const (
	migrationsPath = "file:///root/migrations"
)

// Sink aka a data sink, is a nsq consumer that listens for users.create messages
// and saves the users received to the database.
type Sink struct {
	db          *sqlx.DB
	nsqTopic    string
	nsqChan     string
	nsqAddr     string
	maxInFlight int
}

// New returns a new sink.
func New(db *sqlx.DB, topic, channel, address string, maxInFlight int) (*Sink, error) {
	// Run migrations
	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create postgres migration driver")
	}

	m, err := migrate.NewWithDatabaseInstance(migrationsPath, "postgres", driver)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create migration database instance")
	}
	if err := m.Up(); err != nil && !strings.Contains(err.Error(), "no change") {
		return nil, errors.Wrap(err, "Failed to run database migrations")
	}

	logrus.Info("Migrations performed successfully")

	return &Sink{
		db:          db,
		nsqTopic:    topic,
		nsqChan:     channel,
		nsqAddr:     address,
		maxInFlight: maxInFlight,
	}, nil
}

// Consume blocks waiting for messages.
func (s *Sink) Consume() error {
	consumer, err := nsq.StartConsumer(nsq.ConsumerConfig{
		Topic:       s.nsqTopic,
		Channel:     s.nsqChan,
		Lookup:      []string{s.nsqAddr},
		MaxInFlight: s.maxInFlight,
	})
	if err != nil {
		return errors.Wrap(err, "failed to initialize consumer")
	}

	for msg := range consumer.Messages() {
		mqUser := mq.UsersCreate{}
		if err := json.Unmarshal(msg.Body, &mqUser); err != nil {
			logrus.WithError(err).WithField("raw", msg.Body).Error("Failed to unmarshal message")
			msg.Finish()
			continue
		}

		log := logrus.WithFields(logrus.Fields{
			"name":  mqUser.Name,
			"email": mqUser.Email,
		})

		// TODO check email regex

		ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(time.Minute))
		defer cancel()

		res, err := s.db.ExecContext(ctx, `
			INSERT INTO public.users
				(name, email)
			VALUES
				($1, $2)
			RETURNING id
		`, mqUser.Name, mqUser.Email)
		if err != nil {
			// TODO check for conflict error and report it
			log.WithError(err).Error("Failed to insert into DB")
			msg.Finish()
			continue
		}

		id, err := res.LastInsertId()
		if err != nil {
			log.WithError(err).Warn("Failed to get user ID after INSERT")
		} else {
			log = log.WithField("id", id)
		}

		log.Info("Saved new user to db")
		msg.Finish()
	}

	logrus.Info("Finished processing messages")
	return nil
}
