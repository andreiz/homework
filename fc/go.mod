module fincompare

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/cenkalti/backoff v2.1.0+incompatible
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/golang-migrate/migrate/v4 v4.1.0
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/segmentio/nsq-go v1.2.2
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	google.golang.org/appengine v1.3.0 // indirect
)
